Deltagare
----------
Johan Lindkvist	930531-9395

Bibliotek
-----------
SDL2-2.0.1
SDL2_image-2.0.0
SDL2_ttf-2.0.12

Bilder
--------
Alla bilder �r ligga i mappet assets


Beskrivning motor
------------------
Gameloop: Min loop fungerar s� att man anger "tick rate" dvs antal updateringar per sekund och sen k�rs motorn p� det.
Om man s�tter tickrate till 30 s� den att kalla p� tick 30 ggr per sekund men kommer fortfarande kalla render MAX 500 ggr sekund och d� interpolerar
motorn grafiken.

Grafik: P� det s�ttet jag har gjort �r att Entity som �r min basklass f�r objekt har ett Graphic objeckt och det finns flera
subklasser till Graphic som tex Sprite, d� i Entity kallar man setGraphic(din grafik) s� kommer den rendera det.


Beskrivning spel
-----------------
Spelet �r en breakout klon. Man f�rlorar om alla liv �r borta och vinner om man lyckas f� bort alla brickor.
Spelaren styr plattan med h�ger och v�nster piltangent.
Det finns 2 olika briockor. En som �r statisk och en som r�r sig sidled.
Beroende p� vart bollen tr�ffar plattan s� kommer den �ka olika h�ll.

Startsk�rmen har ett input f�lt som man kan klicka p� och redigera
Plattan har en sprite som grafik (en bild)
Bollen har en animerad sprite som grafik
Brickorna har SDL renderade rektanglar som grafik
