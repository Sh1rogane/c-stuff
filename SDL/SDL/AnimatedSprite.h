#ifndef ANIMATEDSPRITE
#define ANIMATEDSPRITE

#include "SDL.h"
#include "Sprite.h"
#include "GameEngine.h"

class AnimatedSprite : public Sprite
{
	public:
	AnimatedSprite(std::string resource, int frameSize, int frameTime);
	~AnimatedSprite();

	void render(SDL_Renderer* renderer, const SDL_Rect* dstRect, double angle, SDL_RendererFlip& flip);
	int getWidth() const { return this->frameSize; }
	int getHeight() const { return this->frameSize; }
	SDL_Rect& getFrameRect(unsigned int frame);

	const Uint8 getAlphaXY(int x, int y)const;

	private:
	int frameSize;
	SDL_Rect* frameRects;

	unsigned int totalFrames;
	unsigned int currentFrame;
	int frameTime;

	double currentTick;

	bool playing;
	bool repeat;
};
#endif
