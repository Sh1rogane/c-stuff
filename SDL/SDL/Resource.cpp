#include "Resource.h"


Resource::Resource(SDL_Surface* surface, SDL_Texture* texture) : surface(surface), texture(texture)
{
}


Resource::~Resource()
{
	//std::cout << "Resource dtor" << std::endl;
	SDL_FreeSurface(this->surface);
	SDL_DestroyTexture(this->texture);
}
