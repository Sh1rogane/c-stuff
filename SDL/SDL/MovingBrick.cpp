#include "MovingBrick.h"


MovingBrick::MovingBrick(double x, double y) : Entity(x, y, 50, 20), startX(x), dir(1)
{
	setGraphic(new Rectangle((int)width, (int)height, 0, 255, 0, 255));
	setId(3);
}


MovingBrick::~MovingBrick()
{

}

void MovingBrick::tick()
{
	Entity::tick();

	this->x += 100 * GameEngine::getDeltaTickTime() * dir;
	if(x >= startX + 200)
	{
		dir = -1;
	}
	else if(x <= startX)
	{
		dir = 1;
	}
}
