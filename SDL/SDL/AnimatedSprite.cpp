#include "AnimatedSprite.h"


AnimatedSprite::AnimatedSprite(std::string resource, int framesize, int frameTime) :
Sprite(resource), frameSize(framesize), frameTime(frameTime), currentFrame(1), totalFrames(0)
{
	totalFrames = (this->resource->getSurface()->w / framesize) * (this->resource->getSurface()->h / framesize);
	frameRects = new SDL_Rect[totalFrames];
	int num = 0;
	for(int i = 0; i < (this->resource->getSurface()->h / framesize); i++)
	{
		for(int j = 0; j < (this->resource->getSurface()->w / framesize); j++)
		{
			frameRects[num] = {j * frameSize, i * frameSize, frameSize, frameSize};
			num++;
		}
	}
}

AnimatedSprite::~AnimatedSprite()
{
	delete []frameRects;
	frameRects = nullptr;
}
void AnimatedSprite::render(SDL_Renderer* renderer, const SDL_Rect* dstRect, double angle, SDL_RendererFlip& flip)
{
	currentTick += GameEngine::getDeltaTime() * 1000;
	if(currentTick >= frameTime)
	{
		currentTick = 0;
		currentFrame++;
		if(currentFrame > totalFrames)
		{
			currentFrame = 1;
		}
	}
	SDL_RenderCopyEx(renderer, this->resource->getTexture(), &getFrameRect(currentFrame), dstRect, angle, NULL, flip);
}
SDL_Rect& AnimatedSprite::getFrameRect(unsigned int frame)
{
	if(frame > totalFrames || frame == 0)
		return frameRects[0];
	else
		return frameRects[frame - 1];

}

const Uint8 AnimatedSprite::getAlphaXY(int x, int y) const
{
	int bpp = resource->getSurface()->format->BytesPerPixel;
	/* Here p is the address to the pixel we want to retrieve */
	Uint8 *p = (Uint8 *) resource->getSurface()->pixels + (y + frameRects[currentFrame - 1].y) * resource->getSurface()->pitch + (x + frameRects[currentFrame - 1].y) * bpp;

	Uint32 pixelColor;

	switch(bpp)
	{
		case 1:
			pixelColor = *p;
			break;
		case 2:
			pixelColor = *(Uint16 *) p;
			break;
		case 3:
			if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
				pixelColor = p[0] << 16 | p[1] << 8 | p[2];
			else
				pixelColor = p[0] | p[1] << 8 | p[2] << 16;
			break;
		case 4:
			pixelColor = *(Uint32 *) p;
			break;
		default:
			return 0;
	}

	Uint8 r, g, b, a;
	SDL_GetRGBA(pixelColor, resource->getSurface()->format, &r, &g, &b, &a);

	return a;
}