#include "Input.h"
#include <iostream>


Input::Input()
{
}

Input::~Input()
{
}
//Set true if key is pressed, if key not found; add it.
void Input::setKey(const int key, const bool pressed)
{
	std::map<const int, bool>::iterator find = this->keys.find(key);
	if(find != this->keys.end())
	{
		find->second = pressed;
		this->typedKeys[key] = pressed;
	}
	else
	{
		this->keys.insert(std::pair<const int, bool>(key, pressed));
		this->typedKeys.insert(std::pair<const int, bool>(key, pressed));
	}
}
//Return true if key is pressed/down
bool Input::getKeyDown(const int key) const
{
	std::map<const int, bool>::const_iterator find = this->keys.find(key);
	if(find != this->keys.end())
	{
		return find->second;
	}
	return false;
}
bool Input::getKeyTyped(const int key) const
{
	std::map<const int, bool>::const_iterator find = this->typedKeys.find(key);
	if(find != this->typedKeys.end())
	{
		return find->second;
	}
	return false;
}
void Input::setMouse(const int x, const int y)
{
	this->mouseX = x;
	this->mouseY = y;
}
void Input::setMouseDown(const int button, const bool down)
{
	if(this->mouseDown[button] == false)
	{
		this->mouseClicked[button] = down;
	}
	this->mouseDown[button] = down;
}
//Used to reset typed key and mouse click
void Input::tick()
{
	for(std::map<const int, bool>::iterator it = typedKeys.begin(); it != typedKeys.end(); it++)
	{
		it->second = false;
	}

	for(int i = 0; i < 3; i++)
	{
		this->mouseClicked[i] = false;
	}
	textInput = 0;
}
void Input::setInputText(char textInput)
{
	this->textInput = textInput;
}
char Input::getTextInput() const 
{ 
	return this->textInput;
}