#include "BackgroundImage.h"


BackgroundImage::BackgroundImage(std::string file, double x, double y, double width, double height) : Entity(x, y, width, height)
{
	setGraphic(new Sprite(file));
}


BackgroundImage::~BackgroundImage()
{
}
