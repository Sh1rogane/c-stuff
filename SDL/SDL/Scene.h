#ifndef SCENE
#define SCENE

#include "GameEngine.h"
#include "Entity.h"
#include "SP.h"
#include <vector>

class Entity;

class Scene
{
	public:
	Scene();
	virtual ~Scene();

	virtual void tick();
	virtual void init() = 0;
	void render(SDL_Renderer* renderer, double i);

	void addEntity(SP<Entity> e);
	void removeEntity(SP<Entity> e);

	std::vector<SP<Entity>>& getEntitys(int id);

	private:
	std::vector<SP<Entity>> entityList;
	std::vector<SP<Entity>> addList;
	std::vector<SP<Entity>> removeList;

	std::vector<SP<Entity>> returnList;

	void addAndRemoveEntitys();
	void deleteEntity(SP<Entity>& e);
};
#endif
