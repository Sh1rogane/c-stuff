#ifndef TEXTFIELD
#define TEXTFIELD

#include "Entity.h"
#include "Resources.h"
#include "Text.h"

class TextField : public Entity
{
	public:
	
	~TextField();

	void tick();
	void setText(const std::string& text) { this->text->setText(text); }

	void setEditable(bool value) { this->editable = value; }
	bool getEditable() const { return this->editable; }

	void setFocus(bool value) { this->focus = value; }

	const std::string& getText() const { return text->getText(); }

	static TextField* getInstance(double x, double y, std::string text, bool editable, bool hasBg = false, SDL_Color color = { 0, 0, 0, 0 })
	{
		return new TextField(x, y, text, editable, hasBg, color);
	}

	private:
	Text* text;
	bool editable;
	bool focus;
	TextField(const TextField&);
	const TextField& operator=(const TextField&);
	TextField(double x, double y, std::string text, bool editable, bool hasBg = false, SDL_Color color = { 0, 0, 0, 0 });
};
#endif
