#ifndef ENTITY
#define ENTITY

#include "EventManager.h"
#include "GameEngine.h"
#include "SP.h"

enum Event;

class Graphic;
class Scene;

class Entity
{
	friend class Scene;

	public:
		virtual ~Entity();

		virtual void tick();
		void render(SDL_Renderer* renderer, double i);

		void setX(const double x) { this->x = x; this->oldX = x; }
		const double getX()const { return x;}

		void setY(const double y) { this->y = y;  this->oldY = y; }
		const double getY()const { return y; }

		const double getWidth() const { return this->width; }
		const double getHeight() const { return this->height; }

		const double getVelocityX() const { return this->x - oldX; }
		const double getVelocityY() const { return this->y - oldY; }

		void setRotation(const double rotation) { this->rotation = rotation; }
		const double getRotation() const { return this->rotation; }

		void setAlpha(const Uint8 alpha) { this->alpha = alpha; }
		Uint8 getAlpha() const { return this->alpha; }

		const int getId() const { return this->id; }

		void removeSelf();

		const bool checkCollision(const SP<Entity>& other, bool perPixel = false, double vx = 0, double vy = 0, bool otherFuture = false);
		const SDL_Rect getBounds(bool future = false) const;

		const bool contains(int x, int y) const;

		template<class T>
		void addEventListener(Event e, void(T::*callback)()) 
		{
			//std::function<void()> c = std::bind(callback, (T*)this);
			GameEngine::getEventManager().addEventListener(this, e, (void(Entity::*)()) callback);
		}

		template<class T>
		void addKeyListener(SDL_Keycode key, void(T::*callback)())
		{
			GameEngine::getEventManager().addKeyListener(this, key, (void(Entity::*)()) callback);
		}

		template<class T>
		void removeKeyListener(SDL_Keycode key, void(T::*callback)())
		{
			GameEngine::getEventManager().removeKeyListener(this, key, (void(Entity::*)()) callback);
		}

		void clearEventListener();

		const Graphic& getGraphic() const { return *this->graphic; }


		//void addChild(Entity* e);

	private:
		int id;

		double oldX;
		double oldY;
		double oldRotation;

		Graphic* graphic;

		bool remove;

		//std::vector<Entity*> children;

		Entity(const Entity&);
		const Entity& operator=(const Entity&);

		double getInterpolatedX(const double i) const;
		double getInterpolatedY(const double i) const;
		double getInterpolatedRotation(const double i) const;

		SDL_Rect normalizeRect(const SDL_Rect& rect) const;

		void rescaleRect(SDL_Rect& rect);



	protected:
		Entity(double x, double y, double width, double height);

		double x;
		double y;
		double rotation;
		double width;
		double height;
		int alpha;
		bool solid;
		SDL_RendererFlip flip = {SDL_FLIP_NONE};

		void setId(const int id) { this->id = id; }
		void setGraphic(Graphic* graphic);

};

#endif