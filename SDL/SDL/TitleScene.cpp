#include "TitleScene.h"


std::string TitleScene::playerName;

TitleScene::TitleScene()
{
}


TitleScene::~TitleScene()
{
}

void TitleScene::init()
{
	bg = BackgroundImage::getInstance("assets/Title.png", 0, 0);

	textName = TextField::getInstance(300, 400, "Insert name: ", false);
	inputName = TextField::getInstance(textName->getX() + 125, 400, "NAME", true);
	this->addEntity(bg);
	this->addEntity(inputName);
	this->addEntity(textName);
}

void TitleScene::tick()
{
	Scene::tick();

	if(GameEngine::getInput().getKeyTyped(SDLK_RETURN))
	{
		//std::cout << "ENTER! \n";
		TitleScene::playerName = inputName->getText();
		GameEngine::setCurrentScene(new GameScene());
	}
}
