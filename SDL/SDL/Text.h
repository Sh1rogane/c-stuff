#ifndef TEXT
#define TEXT

#include "Graphic.h"

class Text : public Graphic
{
	public:
	Text(std::string text, bool hasBg = false, SDL_Color color = {0,0,0,0});
	~Text();

	void render(SDL_Renderer* renderer, const SDL_Rect* dstRect, double angle, SDL_RendererFlip& flip);

	void setText(const std::string& text);
	const std::string& getText() const { return text; }

	void popText();

	void setFocus(bool value) { this->focus = value; }

	private:
	std::string text;
	SDL_Color color;
	bool hasBg;
	bool focus;
	void updateText();

};

#endif