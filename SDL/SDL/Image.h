#ifndef IMAGE
#define IMAGE

#include <string>
#include "GameEngine.h"

class Image
{
	public:
	Image(std::string file, bool transparent);
	Image(const Image& other);
	Image();
	~Image();

	const Image& operator= (const Image& other);

	SDL_Surface* getSurface() const;


	private:
	SDL_Surface* surface;
};

#endif