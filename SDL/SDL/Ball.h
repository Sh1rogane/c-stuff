#ifndef BALL
#define BALL

#include "Entity.h"
#include "GameScene.h"
#include "AnimatedSprite.h"

class Ball : public Entity
{
	public:
	Ball(double x, double y);
	~Ball(); 

	void tick();

	private:
	double vx;
	double vy;
	bool mercy;
	int mercyTick;
};

#endif