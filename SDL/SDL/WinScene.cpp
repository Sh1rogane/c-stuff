#include "WinScene.h"


WinScene::WinScene()
{
}


WinScene::~WinScene()
{
}

void WinScene::init()
{
	bg = BackgroundImage::getInstance("assets/Win.png", 0, 0);
	text = TextField::getInstance(300, 500, "Player " + TitleScene::getPlayerName() + " got " + std::to_string(GameScene::points), false);
	this->addEntity(bg);
	this->addEntity(text);
}
void WinScene::tick()
{
	Scene::tick();

	if(GameEngine::getInput().getKeyTyped(SDLK_RETURN))
	{
		GameScene::points = 0;
		GameScene::lives = 3;
		GameEngine::setCurrentScene(new GameScene());
	}
}
