#include "Entity.h"
#include "Graphic.h"
#include <string>

Entity::Entity(double x, double y, double width, double height) :
x(x), y(y), width(width), height(height), alpha(0xFF), solid(true), rotation(0), graphic(nullptr)
{

}

Entity::~Entity()
{
	//std::cout << "Entity dtor" << std::endl;
	this->clearEventListener();
	delete this->graphic;
	this->graphic = nullptr;
}
void Entity::clearEventListener()
{
	GameEngine::getEventManager().clearKeyListener(this);
}
void Entity::setGraphic(Graphic* graphic)
{
	delete this->graphic;
	this->graphic = nullptr;

	this->graphic = graphic;
}
void Entity::tick()
{
	//Keeps rotation within 0 - 359 intervall
	if(this->rotation > 359)
		this->rotation -= 359;
	else if(this->rotation < 0)
		this->rotation += 359;

	//This is used to interpolate graphics movements
	this->oldX = this->x;
	this->oldY = this->y;
	this->oldRotation = this->rotation;
}
void Entity::render(SDL_Renderer* renderer, double i)
{
	//Where to render and how big
	SDL_Rect destRect = { (int) this->getInterpolatedX(i), (int) this->getInterpolatedY(i), (int) width, (int) height };
	if(width == 0 || height == 0)
	{
		destRect.w = this->graphic->getWidth();
		destRect.h = this->graphic->getHeight();
	}
	//Render graphic
	if(this->graphic != nullptr)
	{
		this->graphic->render(renderer, &destRect, rotation, flip);
	}
	

	//Draw "hitbox" if debug == true
	if(GameEngine::isDebug() && this->solid)
	{
		//destRect = { (int) x, (int) y, (int) width, (int) height };
		SDL_SetRenderDrawColor(renderer, 0xFF, 0x00, 0xFF, 0xFF);
		SDL_RenderDrawRect(renderer, &destRect);
	}
}
double Entity::getInterpolatedX(double i) const
{
	return this->oldX + (i * (this->x - this->oldX));
}
double Entity::getInterpolatedY(double i) const
{
	return this->oldY + (i * (this->y - this->oldY));
}
double Entity::getInterpolatedRotation(double i) const
{
	return this->oldRotation + (i * (this->rotation - this->oldRotation));
}
void Entity::removeSelf()
{
	this->remove = true;
}
const bool Entity::checkCollision(const SP<Entity>& other, bool perPixel, double vx, double vy, bool otherFuture)
{
	SDL_Rect myBound = this->getBounds(false);
	myBound.x += (int) vx;
	myBound.y += (int) vy;
	SDL_Rect otherBound = other->getBounds(otherFuture);

	if(SDL_HasIntersection(&myBound, &otherBound) == SDL_FALSE)
		return false;

	if(!perPixel)
	{
		return SDL_HasIntersection(&myBound, &otherBound) == SDL_TRUE ? true : false;
	}
	else
	{
		SDL_Rect ir;
		SDL_IntersectRect(&myBound, &otherBound, &ir);

		SDL_Rect myNRect = this->normalizeRect(ir);
		SDL_Rect otherNRect = other->normalizeRect(ir);

		if(this->width != 0 || this->height != 0)
		{
			rescaleRect(ir);
			rescaleRect(myNRect);
			rescaleRect(otherNRect);
		}

		for(int y = 0; y < ir.h; y++)
		{
			for(int x = 0; x < ir.w; x++)
			{
				if(this->graphic->getAlphaXY(myNRect.x + x, myNRect.y + y) > 20 && other->graphic->getAlphaXY(otherNRect.x + x, otherNRect.y + y) > 20)
				{
					return true;
				}
			}
		}
	}
	return false;
}
SDL_Rect Entity::normalizeRect(const SDL_Rect& rect) const
{
	SDL_Rect n;
	n.x = rect.x - (int) (this->x);
	n.y = rect.y - (int) (this->y);
	n.w = rect.w;
	n.h = rect.h;
	//std::cout << n.h << std::endl;
	return n;
}
void Entity::rescaleRect(SDL_Rect& rect)
{
	double scaleX = this->width / this->graphic->getWidth();
	double scaleY = this->height / this->graphic->getHeight();

	rect.x = (int)(rect.x / scaleX);
	rect.y = (int)(rect.y / scaleY);
	rect.w = (int)(rect.w / scaleX);
	rect.h = (int)(rect.h / scaleY);
}
const SDL_Rect Entity::getBounds(bool future) const
{
	SDL_Rect bound = { (int) x, (int) y, (int) width, (int) height };
	if(width == 0 || height == 0)
	{
		bound.w = this->graphic->getWidth();
		bound.h = this->graphic->getHeight();
	}
	if(future)
	{
		bound.x += (int)this->getVelocityX();
		bound.y += (int)this->getVelocityY();
	}
	return bound;
}

const bool Entity::contains(int x, int y) const
{
	SDL_Rect rect = getBounds();

	return (x > rect.x && x < rect.x + rect.w && y > rect.y && y < rect.y + rect.h);
}