#include "EventManager.h"
#include "GameEngine.h"
#include "Entity.h"
#include "Input.h"

EventManager::EventManager(GameEngine* game) : game(game)
{
	input = new Input();
}


EventManager::~EventManager()
{
	delete input;
	input = nullptr;

	keyMap.clear();
}

void EventManager::tick()
{
	//Loop for anonymous key events
	for(std::pair<SDL_Keycode, ACallback> ae : aKeyMap)
	{
		if(input->getKeyDown(ae.first))
		{
			ae.second();
		}
	}
	//Loop for key events
	for(KeyMapPair kmp : keyMap)
	{
		for(KeyCodePair kcp : kmp.second)
		{
			for(Callback c : kcp.second)
			{
				if(input->getKeyDown(kcp.first))
				{
					(kmp.first->*c)();
				}
			}
		}
	}

	input->tick();
	hasKeyboardEvents = false;
	hasMouseEvents = false;
}

void EventManager::handleEvents(SDL_Event& e)
{
	while(SDL_PollEvent(&e))
	{
		switch(e.type)
		{
			case SDL_QUIT:
				game->stop();
				break;
			case SDL_KEYDOWN:
			case SDL_KEYUP:
				input->setKey(e.key.keysym.sym, e.key.state == SDL_PRESSED ? true : false);
				hasKeyboardEvents = true;
				break;
			case SDL_MOUSEMOTION:
				input->setMouse(e.motion.x, e.motion.y);
				break;
			case SDL_MOUSEBUTTONDOWN:
			case SDL_MOUSEBUTTONUP:
				hasMouseEvents = true;
				input->setMouseDown(e.button.button, e.button.state == SDL_PRESSED ? true : false);
				break;
			case SDL_TEXTINPUT:
				//std::cout << e.text.text << std::endl;
				input->setInputText(*e.text.text);
				break;
			case SDL_TEXTEDITING:
				std::cout << e.edit.text << std::endl;
				break;
			default:
				break;
		}
	}
}

void EventManager::addKeyListener(SDL_Keycode key, ACallback callback)
{
	aKeyMap.insert(std::pair<SDL_Keycode, ACallback>(key, callback));
}

void EventManager::addKeyListener(Entity* e, SDL_Keycode key, Callback callback)
{
	KeyMap::iterator find = keyMap.find(e);

	if(find != keyMap.end())
	{
		KeycodeMap::iterator find2 = find->second.find(key);

		if(find2 != find->second.end())
		{
			find2->second.push_back(callback);
		}
		else
		{
			std::vector<Callback> vector;
			vector.push_back(callback);
			find->second.insert(std::pair<SDL_Keycode, std::vector<Callback> >(key, vector));
		}
	}
	else
	{
		KeycodeMap map;
		std::vector<Callback> vector;
		vector.push_back(callback);
		map.insert(std::pair<SDL_Keycode, std::vector<Callback> >(key, vector));
		keyMap.insert(std::pair<Entity*, KeycodeMap>(e, map));
	}
}
void EventManager::removeKeyListener(Entity* e, SDL_Keycode key, Callback callback)
{
	KeyMap::iterator find = keyMap.find(e);

	if(find != keyMap.end())
	{
		KeycodeMap::iterator find2 = find->second.find(key);

		if(find2 != find->second.end())
		{
			std::vector<Callback>::iterator find3 = std::find(find2->second.begin(), find2->second.end(), callback);

			if(find3 != find2->second.end())
			{
				find2->second.erase(find3);
			}
		}
	}
}
void EventManager::clearKeyListener(Entity* e)
{
	KeyMap::iterator find = keyMap.find(e);
	if(find != keyMap.end())
	{
		keyMap.erase(find);
	}
}