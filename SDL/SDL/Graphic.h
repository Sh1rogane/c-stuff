#ifndef GRAPHIC
#define GRAPHIC

#include "SDL.h"
#include <string>
#include "GameEngine.h"

class Graphic
{
	public:
	virtual ~Graphic();

	virtual void render(SDL_Renderer* renderer, const SDL_Rect* dstRect, double angle, SDL_RendererFlip& flip) = 0;

	virtual int getHeight() const { return this->resource->getSurface()->h; }
	virtual int getWidth() const { return this->resource->getSurface()->w; }

	//Resource* getResource() const { return this->resource; }

	virtual const Uint8 getAlphaXY(int x, int y)const;

	private:

	protected:
	Graphic(std::string resource);
	Graphic(Resource* resource);
	Graphic();
	Resource* resource;
};

#endif