#include "GameEngine.h"
#include <ctime>
#include "TitleScene.h"

int main(int argc, char** argv)
{
	srand((unsigned int) time(0));

	GameEngine* game = new GameEngine(800, 600, 30);
	game->setCurrentScene(new TitleScene());
	game->start();

	return 0;
}

