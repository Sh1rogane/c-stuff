#include "Image.h"


Image::Image(std::string file, bool transparent)
{
	this->surface = IMG_Load(file.c_str());
	if(surface == nullptr)
	{
		throw std::runtime_error("No image found in: " + file + IMG_GetError());
	}
	if(transparent)
	{
		Uint32 pixelColor = (Uint32) surface->pixels;
		SDL_SetColorKey(this->surface, SDL_TRUE, pixelColor);
	}
}

Image::Image()
{

}
Image::Image(const Image& other) : surface(other.surface)
{
	this->surface->refcount++;
}

Image::~Image()
{
	SDL_FreeSurface(this->surface);
}
const Image& Image::operator = (const Image& other)
{
	if(this->surface != other.surface)
	{
		SDL_FreeSurface(this->surface);

		this->surface = other.surface;
		this->surface->refcount++;
	}
	return *this;
}
SDL_Surface* Image::getSurface() const
{
	return this->surface;
}
