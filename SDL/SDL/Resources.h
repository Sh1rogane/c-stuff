#ifndef RESOURCES
#define RESOURCES

#include "GameEngine.h"
#include <map>
#include "Resource.h"

class Resources
{
	public:
	Resources(SDL_Renderer* renderer);
	~Resources();

	Resource* getResource(const std::string& name);
	void loadResource(const std::string& resource);
	void unloadResource(const std::string& resource);
	void clearResources();

	Resource* getRenderedText(const std::string& text);

	private:
	std::map<const std::string, Resource*> resources;
	SDL_Renderer* renderer;
	TTF_Font* font;
};

#endif