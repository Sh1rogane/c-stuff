#include "GameScene.h"

int GameScene::points = 0;
int GameScene::lives = 3;

GameScene::GameScene()
{
	points = 0;
	lives = 3;
}

GameScene::~GameScene()
{
}

void GameScene::init()
{
	paddle = new Paddle(400, 550);
	ball = new Ball(400, 400);
	score = TextField::getInstance(20, 200, "SCORE", false);
	life = TextField::getInstance(20, 250, "LIFE", false);

	this->addEntity(paddle);
	this->addEntity(ball);



	for(int y = 0; y < 3; y++)
	{
		for(int x = 0; x < 12; x++)
		{
			this->addEntity(new Brick(40 + (x * 60), 30 + (y * 30)));
		}
	}

	for(int x = 0; x < 5; x++)
	{
		this->addEntity(new MovingBrick(40 + (x * 100), 200));
	}
	this->addEntity(score);
	this->addEntity(life);
}
void GameScene::tick()
{
	Scene::tick();

	score->setText("Score: " + std::to_string(points));
	life->setText("Lives: " + std::to_string(lives));

	if(lives <= 0)
	{
		GameEngine::setCurrentScene(new LoseScene());
	}
	else if(GameEngine::getCurrentScene().getEntitys(3).size() <= 0)
	{
		GameEngine::setCurrentScene(new WinScene());
	}
}
