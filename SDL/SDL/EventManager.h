#ifndef EVENTMANAGER
#define EVENTMANAGER

#include "SDL.h"
#include <map>
#include <functional>
#include <vector>

class Entity;
class GameEngine;
class Input;

class EventManager
{
	typedef void(Entity::*Callback)();

	typedef void(*ACallback)();

	typedef std::map<SDL_Keycode, std::vector<Callback> > KeycodeMap;
	typedef std::map<Entity*, KeycodeMap> KeyMap;

	typedef std::pair<Entity*, KeycodeMap> KeyMapPair;
	typedef std::pair<SDL_Keycode, std::vector<Callback> > KeyCodePair;


	public:
	EventManager(GameEngine* game);
	~EventManager();

	void handleEvents(SDL_Event& e);

	void tick();

	//Function for anomynos callbacks
	void addKeyListener(SDL_Keycode key, ACallback callback);

	void addKeyListener(Entity* e, SDL_Keycode key, Callback callback);
	void removeKeyListener(Entity* e, SDL_Keycode key, Callback callback);
	void clearKeyListener(Entity* e);

	const Input& getInput() { return *this->input; }

	private:
	GameEngine* game;
	Input* input;

	KeyMap keyMap;

	std::map<SDL_Keycode, ACallback> aKeyMap;

	bool hasMouseEvents;
	bool hasKeyboardEvents;
};
#endif
