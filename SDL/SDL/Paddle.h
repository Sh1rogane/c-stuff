#ifndef PADDLE
#define PADDLE

#include "Entity.h"
#include "Sprite.h"
class Paddle : public Entity
{
	public:
	Paddle(double x, double y);
	~Paddle();

	void tick();

	private:
	void moveLeft();
	void moveRight();
};

#endif