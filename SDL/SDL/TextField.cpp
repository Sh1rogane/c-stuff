#include "TextField.h"


TextField::TextField(double x, double y, std::string text, bool editable, bool hasBg, SDL_Color color) : Entity(x, y, 0, 0), focus(false), editable(editable)
{
	this->text = new Text(text, hasBg, color);
	this->setGraphic(this->text);
}

TextField::~TextField()
{

}

void TextField::tick()
{
	Entity::tick();
	if(focus)
	{
		if(GameEngine::getInput().getTextInput() != 0)
		{
			this->setText(getText() + GameEngine::getInput().getTextInput());
		}
		else if(GameEngine::getInput().getKeyTyped(SDLK_BACKSPACE) && getText().size() > 0)
		{
			this->text->popText();
		}
	}
	if(editable && GameEngine::getInput().getMouseClicked(SDL_BUTTON_LEFT))
	{
		if(this->contains(GameEngine::getInput().getMouseX(), GameEngine::getInput().getMouseY()))
		{
			focus = true;
			text->setFocus(focus);
		}
		else
		{
			focus = false;
			text->setFocus(focus);
		}
	}

}
