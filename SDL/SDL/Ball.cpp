#include "Ball.h"


Ball::Ball(double x, double y) : Entity(x, y, 20, 20), vx(5), vy(5), mercy(false), mercyTick(0)
{
	setGraphic(new AnimatedSprite("assets/Ball.png", 16, 1000 / 14));
}


Ball::~Ball()
{
}

void Ball::tick()
{
	Entity::tick();
	this->x += vx;
	this->y += vy;

	this->mercyTick++;
	if(this->mercyTick >= 2)
	{
		mercyTick = 0;
		this->mercy = false;
	}

	if(this->x + this->width + vx >= GameEngine::getWidth() || this->x + vx <= 0)
	{
		if(!mercy)
		{
			mercy = true;
			vx *= -1;
		}
			
	}
	if(this->y <= 0)
	{
		vy *= -1;
	}
	else if(this->y + this->height >= GameEngine::getHeight())
	{
		GameScene::lives--;
		this->setX(400);
		this->setY(400);
		this->vx = 5;
		this->vy = 5;
	}
	if(!mercy)
	{
		for(SP<Entity>& e : GameEngine::getCurrentScene().getEntitys(1))
		{
			if(this->checkCollision(e, true, vx, vy, true))
			{
				double nx = (e->getX() - this->x + (e->getWidth() / 2) - (this->width / 2)) *-1;
				vy *= -1;
				vx = nx / 5.0;
				this->mercy = true;
				break;
			}
		}
	}
	
	for(SP<Entity>& e : GameEngine::getCurrentScene().getEntitys(3))
	{
		if(this->checkCollision(e, true, vx, vy))
		{
			//vy *= -1;

			if(this->x - vx >= e->getX() + e->getWidth())
			{
				vx *= -1;
			}
			else if(this->x - vx + this->width <= e->getX())
			{
				vx *= -1;
			}
			else
			{
				vy *= -1;
			}
			e->removeSelf();
			GameScene::points += 100;
			break;
		}
	}
}
