#include "LoseScene.h"


LoseScene::LoseScene()
{
}


LoseScene::~LoseScene()
{
}

void LoseScene::init()
{
	bg = BackgroundImage::getInstance("assets/Lose.png", 0, 0);
	text = TextField::getInstance(300, 400, "Player " + TitleScene::getPlayerName() + " got " + std::to_string(GameScene::points), false);
	this->addEntity(bg);
	this->addEntity(text);
}
void LoseScene::tick()
{
	Scene::tick();

	if(GameEngine::getInput().getKeyTyped(SDLK_RETURN))
	{
		GameScene::points = 0;
		GameScene::lives = 3;
		GameEngine::setCurrentScene(new GameScene());
	}
}
