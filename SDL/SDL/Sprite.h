#ifndef SPRITE
#define SPRITE

#include "Graphic.h"

class Sprite : public Graphic
{
	public:
	Sprite(std::string resource);
	~Sprite();

	virtual void render(SDL_Renderer* renderer, const SDL_Rect* dstRect, double angle, SDL_RendererFlip& flip);

	private:

};
#endif
