#ifndef RECTANGLE
#define RECTANGLE

#include "Graphic.h"

class Rectangle : public Graphic
{
	public:
	Rectangle(int width, int height, SDL_Color color);
	Rectangle(int width, int height, Uint8 r, Uint8 g, Uint8 b, Uint8 a);
	~Rectangle();

	void render(SDL_Renderer* renderer, const SDL_Rect* dstRect, double angle, SDL_RendererFlip& flip);
	int getWidth() const { return this->width; }
	int getHeight() const { return this->height; }

	void setColor(SDL_Color color) { this->color = color; }
	void setColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a) { color.r = r; color.g = g; color.b = b; color.a = a; }

	const Uint8 getAlphaXY(int x, int y) const;

	private:
	int width;
	int height;
	SDL_Color color;
};

#endif