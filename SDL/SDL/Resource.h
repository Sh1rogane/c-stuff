#ifndef RESOURCE
#define RESOURCE

#include "SDL.h"
#include <iostream>

class Resource
{
	public:
	Resource(SDL_Surface* surface, SDL_Texture* texture);
	~Resource();

	SDL_Texture* getTexture() { return this->texture; }
	SDL_Surface* getSurface() { return this->surface; }

	private:
	SDL_Texture* texture;
	SDL_Surface* surface;
};
#endif
