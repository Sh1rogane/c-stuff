#ifndef GAMESCENE
#define GAMESCENE


#include "Scene.h"
#include "Paddle.h"
#include "Ball.h"
#include "Brick.h"
#include "TextField.h"
#include "MovingBrick.h"
#include "LoseScene.h"
#include "WinScene.h"

class Ball;

class GameScene : public Scene
{
	public:
	GameScene();
	~GameScene();

	void init();
	void tick();

	static int points;
	static int lives;

	private:
	SP<Paddle> paddle;
	SP<Ball> ball;
	SP<TextField> score;
	SP<TextField> life;
};

#endif