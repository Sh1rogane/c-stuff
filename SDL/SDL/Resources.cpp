#include "Resources.h"


Resources::Resources(SDL_Renderer* renderer) : renderer(renderer)
{
	font = TTF_OpenFont("assets/ARIAL.TTF", 20);
}


Resources::~Resources()
{
	std::cout << "Resources dtor" << std::endl;
	clearResources();
	TTF_CloseFont(font);
}
//Returns a Resource object if resource is loaded, else it will be loaded and then returned
Resource* Resources::getResource(const std::string& name)
{
	std::map<const std::string, Resource*>::iterator find = resources.find(name);
	if(find != resources.end())
	{
		
		return find->second;
	}
	else
	{
		//LOAD AND ERROR CHECK
		SDL_Surface* surface = IMG_Load(name.c_str());
		if(surface == nullptr)
		{
			throw std::runtime_error("No image found in: " + name + IMG_GetError());
		}
		SDL_Texture* texture = SDL_CreateTextureFromSurface(this->renderer, surface);
		Resource* resource = new Resource(surface, texture);
		resources.insert(std::pair<const std::string, Resource*>(name, resource));
		return resource;
	}
}
void Resources::loadResource(const std::string& resource)
{
	this->getResource(resource);
}
void Resources::unloadResource(const std::string& resource)
{
	std::map<const std::string, Resource*>::iterator find = resources.find(resource);
	for(std::pair<const std::string, Resource*> e : resources)
	{
		delete e.second;
		resources.erase(find);
	}
}
void Resources::clearResources()
{
	for(std::pair<const std::string, Resource*> e : resources)
	{
		delete e.second;
	}
	resources.clear();
	std::cout << "clearing \n";
}

Resource* Resources::getRenderedText(const std::string& text)
{
	SDL_Color color = { 255, 255, 255 };
	SDL_Surface* s = TTF_RenderText_Blended(this->font, text.c_str(), color);
	if(s == nullptr)
		s = TTF_RenderText_Blended(this->font, " ", color);

	SDL_Texture* t = SDL_CreateTextureFromSurface(this->renderer, s);
	return new Resource(s, t);
}
