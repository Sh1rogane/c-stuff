#ifndef INPUT	
#define INPUT


#include "GameEngine.h"
#include<map>

class Input
{
	public:
		Input();
		~Input();

		bool getKeyDown(const int key) const;
		bool getKeyTyped(const int key) const;
		void setKey(const int key, const bool pressed);

		bool getMouseDown(const int button) const { return this->mouseDown[button]; }
		bool getMouseClicked(const int button) const { return this->mouseClicked[button]; }
		int getMouseX() const { return this->mouseX; }
		int getMouseY() const { return this->mouseY; }
		char getTextInput() const;


		void setMouse(const int x, const int y);
		void setMouseDown(const int button, const bool down);
		void setInputText(char textInput);

		void tick();

	private:
		std::map<const int, bool> keys;
		std::map<const int, bool> typedKeys;

		int mouseX;
		int mouseY;

		bool mouseDown[3];
		bool mouseClicked[3];

		char textInput;



};

#endif