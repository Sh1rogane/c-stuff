#ifndef SP_H
#define SP_H

template<typename T>
class SP
{
	public:
		SP(T* p = 0) : t(p)
		{
			//std::cout << "ctor1 \n";
			if(t)
			{
				this->counter = new int(1);
			}
			else
			{
				this->counter = new int(0);
			}
		}
		SP(const SP& sp) : t(sp.t), counter(sp.counter)
		{
			//std::cout << "SP ctor2 \n";
			++*counter;
		}

		~SP()
		{
			//std::cout << "Smart pointer dtor \n";
			if(--*counter == 0)
			{
				//std::cout << "Smart pointer dtor Removed pointer \n";
				delete this->t;
				delete this->counter;
			}
		}

		template <typename S>
		operator SP<S>()
		{
			return SP<S>(t, counter);
		}


		T* operator->() const { return t; }
		T& operator*() const { return *t; }
		T* operator&() { return t; }
		bool operator==(const SP& other)
		{
			if(this->t == other.t)
			{
				return true;
			}
			return false;
		}
		/*bool operator<(const SP& other) const
		{
			std::cout << "compate! \n";
			return this->t < other->t;
		}*/
		const SP& operator= (const SP& sp)
		{
			if(this->t != sp.t)
			{
				if(t && --*counter == 0)
				{
					delete this->t;
					delete this->counter;
				}
				this->t = sp.t;
				this->counter = sp.counter;
				++*counter;
			}
			return *this;
		}

	private:
		T* t;
		int* counter;

		template <typename X>
		friend class SP;

		SP(T* p, int* c) : t(p), counter(c)
		{
			//std::cout << "SP ctor3 \n";
			++*counter;
		}
};

#endif