#ifndef MOVINGBRICK
#define MOVINGBRICK

#include "Entity.h"
#include "Rectangle.h"

class MovingBrick : public Entity
{
	public:
	MovingBrick(double x, double y);
	~MovingBrick();

	void tick();

	private:
	double startX;
	int dir;
};

#endif