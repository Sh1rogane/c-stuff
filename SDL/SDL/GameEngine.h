#ifndef GAMEENGINE
#define GAMEENGINE

#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"
#include "Resources.h"
#include "EventManager.h"
#include "Scene.h"
#include "Input.h"
#include <iostream>

class Scene;
class Resources;
class Input;
class EventManager;

class GameEngine
{
	public:
		GameEngine(int width, int height, int tps);
		~GameEngine();
		void start();
		void stop();

		static const Input& getInput();
		static Scene& getCurrentScene() { return *GameEngine::currentScene; }
		static Resources& getResources() { return *GameEngine::resources; }
		static EventManager& getEventManager() { return *GameEngine::eventMng; }

		static double getDeltaTime() { return GameEngine::delta / 1000.0; }
		static double getDeltaTickTime() { return GameEngine::tickDelta / 1000.0; }

		static void setCurrentScene(Scene* scene);

		static const int getWidth() { return GameEngine::gameWidth; }
		static const int getHeight() { return GameEngine::gameHeight; }
		static const bool isDebug() { return GameEngine::debug; }
		static void setDebug(const bool value) { GameEngine::debug = value; }

		

	private:
		SDL_Window* window;
		SDL_Renderer* renderer;

		static Scene* currentScene;
		static Resources* resources;
		static EventManager* eventMng;

		static int gameWidth;
		static int gameHeight;

		static bool debug;

		static int delta;
		static int tickDelta;

		bool running;

		int ticksPerSec;
		int skipTicks;

		void gameloop();
		void handleEvents(SDL_Event* e);
		void tick();
		void render(double i);

		void renderText(int x, int y, const char* text, SDL_Color color, int size);

};

#endif