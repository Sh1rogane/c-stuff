#pragma once
#include "Entity.h"
class Particle : public Entity
{
	public:
		Particle(double x, double y, double vx, double vy);
		~Particle();

		void tick();

	private:
		double vx;
		double vy;

};

