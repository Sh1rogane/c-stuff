#include "Scene.h"


Scene::Scene()
{

}

Scene::~Scene()
{
	std::cout << "Scene dtor" << std::endl;

	GameEngine::getResources().clearResources();
	this->addList.clear();
	this->removeList.clear();
	this->entityList.clear();
	this->returnList.clear();
}
void Scene::tick()
{
	//std::cout << entityList.size() << std::endl;
	this->addAndRemoveEntitys();

	for(SP<Entity>& e : this->entityList)
	{
		e->tick();
	}
}
void Scene::render(SDL_Renderer* renderer, double i)
{
	for(SP<Entity>& e : this->entityList)
	{
		e->render(renderer, i);
	}
}

void Scene::addEntity(SP<Entity> e)
{
	this->addList.push_back(e);
}
void Scene::removeEntity(SP<Entity> e)
{
	this->removeList.push_back(e);
}
//Add and remove entitys before calling tick
void Scene::addAndRemoveEntitys()
{
	for(SP<Entity>& e : this->entityList)
	{
		if(e->remove)
		{
			this->removeEntity(e);
		}
	}
	//std::cout << this->entityList.size() << std::endl;
	if(this->addList.size() > 0)
	{
		for(SP<Entity>& e : this->addList)
		{
			this->entityList.push_back(e);
		}
		this->addList.clear();
	}
	if(this->removeList.size() > 0)
	{
		for(SP<Entity>& e : this->removeList)
		{
			this->deleteEntity(e);
		}
		this->removeList.clear();
	}
}
void Scene::deleteEntity(SP<Entity>& e)
{
	std::vector<SP<Entity>>::iterator pos = std::find(this->entityList.begin(), this->entityList.end(), e);
	if(pos != this->entityList.end())
	{
		//delete * pos;
		this->entityList.erase(pos);
	}
}
std::vector<SP<Entity>>& Scene::getEntitys(int id)
{
	if(id == 0)
	{
		return this->entityList;
	}
	else
	{
		returnList.clear();
		for(SP<Entity>& e : this->entityList)
		{
			if(e->getId() == id)
			{
				returnList.push_back(e);
			}
		}
		return returnList;
	}
}