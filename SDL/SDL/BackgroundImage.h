#ifndef BACKGROUNDIMAGE
#define BACKGROUNDIMAGE

#include "Entity.h"
#include "Sprite.h"
class BackgroundImage :	public Entity
{
	public:
	
	~BackgroundImage();

	static BackgroundImage* getInstance(std::string file, double x, double y, double width = 0, double height = 0)
	{
		return new BackgroundImage(file, x, y, width, height);
	}

	private:
	BackgroundImage(std::string file, double x, double y, double width = 0, double height = 0);

	BackgroundImage(const BackgroundImage&);
	const BackgroundImage& operator=(const BackgroundImage&);
};

#endif