#include "Text.h"

Text::Text(std::string text, bool hasBg, SDL_Color color) : Graphic(GameEngine::getResources().getRenderedText(text)), text(text),
hasBg(hasBg), color(color)
{

}
Text::~Text()
{
	delete this->resource;
	this->resource = nullptr;
}
void Text::render(SDL_Renderer* renderer, const SDL_Rect* dstRect, double angle, SDL_RendererFlip& flip)
{
	if(hasBg)
	{
		SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
		SDL_RenderFillRect(renderer, dstRect);
	}
	if(focus)
	{
		SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
		SDL_RenderDrawRect(renderer, dstRect);
	}
	SDL_RenderCopyEx(renderer, this->resource->getTexture(), NULL, dstRect, angle, NULL, flip);
}

void Text::setText(const std::string& text)
{
	this->text = text;
	this->updateText();
}
void Text::popText()
{
	this->text.pop_back();
	this->updateText();
}
void Text::updateText()
{
	delete this->resource;
	this->resource = GameEngine::getResources().getRenderedText(text);
}


