#ifndef WINSCENE
#define WINSCENE

#include "Scene.h"
#include "BackgroundImage.h"
#include "TextField.h"
#include "TitleScene.h"
#include "GameScene.h"

class WinScene : public Scene
{
	public:
	WinScene();
	~WinScene();
	void init();
	void tick();

	private:
	SP<BackgroundImage> bg;
	SP<TextField> text;
};
#endif
