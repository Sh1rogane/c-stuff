#ifndef LOSESCENE
#define LOSESCENE

#include "Scene.h"
#include "BackgroundImage.h"
#include "TextField.h"
#include "TitleScene.h"
#include "GameScene.h"

class LoseScene : public Scene
{
	public:
	LoseScene();
	~LoseScene();
	void init();
	void tick();

	private:
	SP<BackgroundImage> bg;
	SP<TextField> text;
};

#endif