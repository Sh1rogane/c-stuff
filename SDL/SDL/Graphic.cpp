#include "Graphic.h"

Graphic::Graphic(std::string resource) : resource(GameEngine::getResources().getResource(resource))
{

}
Graphic::Graphic(Resource* resource) : resource(resource)
{

}
Graphic::Graphic()
{

}
Graphic::~Graphic()
{

}

const Uint8 Graphic::getAlphaXY(int x, int y) const
{
	int bpp = resource->getSurface()->format->BytesPerPixel;
	/* Here p is the address to the pixel we want to retrieve */
	Uint8 *p = (Uint8 *) resource->getSurface()->pixels + y * resource->getSurface()->pitch + x * bpp;

	Uint32 pixelColor;

	switch(bpp)
	{
		case 1:
			pixelColor = *p;
			break;
		case 2:
			pixelColor = *(Uint16 *) p;
			break;
		case 3:
			if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
				pixelColor = p[0] << 16 | p[1] << 8 | p[2];
			else
				pixelColor = p[0] | p[1] << 8 | p[2] << 16;
			break;
		case 4:
			pixelColor = *(Uint32 *) p;
			break;
		default:
			return 0;
	}

	Uint8 r, g, b, a;
	SDL_GetRGBA(pixelColor, resource->getSurface()->format, &r, &g, &b, &a);

	return a;
}
