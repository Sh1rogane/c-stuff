#include "Rectangle.h"


Rectangle::Rectangle(int width, int height, SDL_Color color) : width(width), height(height), color(color)
{

}
Rectangle::Rectangle(int width, int height, Uint8 r, Uint8 g, Uint8 b, Uint8 a) : width(width), height(height), color({r,g,b,a})
{

}

Rectangle::~Rectangle()
{

}

void Rectangle::render(SDL_Renderer* renderer, const SDL_Rect* dstRect, double angle, SDL_RendererFlip& flip)
{
	SDL_Rect rect = { dstRect->x, dstRect->y, width, height };
	SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
	SDL_RenderFillRect(renderer, &rect);
}

const Uint8 Rectangle::getAlphaXY(int x, int y) const
{
	return 255;
}