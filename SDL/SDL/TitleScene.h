#ifndef TITLESCENE
#define TITLESCENE

#include "Scene.h"
#include "BackgroundImage.h"
#include "GameScene.h"
class TitleScene : public Scene
{
	public:
	TitleScene();
	~TitleScene();

	void init();
	void tick();

	static std::string& getPlayerName() { return playerName; }

	private:
	SP<BackgroundImage> bg;
	SP<TextField> textName;
	SP<TextField> inputName;
	static std::string playerName;
};

#endif