#include "Paddle.h"


Paddle::Paddle(double x, double y) : Entity(x, y, 75, 20)
{
	setGraphic(new Sprite("assets/Paddle.png"));
	this->addKeyListener(SDLK_LEFT, &Paddle::moveLeft);
	this->addKeyListener(SDLK_RIGHT, &Paddle::moveRight);

	this->setId(1);
}


Paddle::~Paddle()
{
}

void Paddle::tick()
{
	Entity::tick();
}

void Paddle::moveLeft()
{
	if(this->x - 10 < 0)
	{
		this->x = 0;
	}
	else
	{
		this->x -= 10;
	}
	
}
void Paddle::moveRight()
{
	if(this->x + this->width + 10 > GameEngine::getWidth())
	{
		this->x = GameEngine::getWidth() - this->width;
	}
	else
	{
		this->x += 10;
	}
}
