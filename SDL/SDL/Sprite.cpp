#include "Sprite.h"


Sprite::Sprite(std::string resource) : Graphic(resource)
{
}

Sprite::~Sprite()
{

}

void Sprite::render(SDL_Renderer* renderer, const SDL_Rect* dstRect, double angle, SDL_RendererFlip& flip)
{
	SDL_RenderCopyEx(renderer, this->resource->getTexture(), NULL, dstRect, angle, NULL, flip);
}
