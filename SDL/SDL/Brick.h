#ifndef BRICK
#define BRICK

#include "Entity.h"
#include "Rectangle.h"

class Brick : public Entity
{
	public:
	Brick(double x, double y);
	~Brick();
};

#endif