#include "GameEngine.h"
#include <string>

Scene* GameEngine::currentScene = nullptr;
Resources* GameEngine::resources = nullptr;
EventManager* GameEngine::eventMng = nullptr;


int GameEngine::gameHeight = 0;
int GameEngine::gameWidth = 0;

bool GameEngine::debug = false;

int GameEngine::delta = 0;
int GameEngine::tickDelta = 0;

const Input& GameEngine::getInput()
{ 
	return GameEngine::eventMng->getInput(); 
}

GameEngine::GameEngine(int width, int height, int tps) : ticksPerSec(tps)
{
	if(SDL_Init(SDL_INIT_EVERYTHING) == -1)
	{
		throw std::runtime_error(SDL_GetError());
	}
	
	if(IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG) == -1)
	{
		throw std::runtime_error(SDL_GetError());
	}
	if(TTF_Init() == -1)
	{
		throw std::runtime_error(SDL_GetError());
	}

	this->skipTicks = 1000 / this->ticksPerSec;
	GameEngine::gameWidth = width;
	GameEngine::gameHeight = height;

	this->window = SDL_CreateWindow("GameEngine", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
	if(window == nullptr)
	{
		throw std::runtime_error(SDL_GetError());
	}
	this->renderer = SDL_CreateRenderer(this->window, -1, SDL_RENDERER_ACCELERATED);
	if(renderer == nullptr)
	{
		throw std::runtime_error(SDL_GetError());
	}
	SDL_RenderSetLogicalSize(this->renderer, width, height);
	SDL_SetRenderDrawBlendMode(this->renderer, SDL_BLENDMODE_BLEND);
	this->running = false;

	GameEngine::resources = new Resources(this->renderer);
	GameEngine::eventMng = new EventManager(this);
}

GameEngine::~GameEngine()
{
	std::cout << "GameEngine dtor" << std::endl;
	delete GameEngine::currentScene;
	GameEngine::currentScene = nullptr;
	delete GameEngine::resources;
	GameEngine::resources = nullptr;
	delete GameEngine::eventMng;
	GameEngine::eventMng = nullptr;
	SDL_DestroyRenderer(this->renderer);
	SDL_DestroyWindow(this->window);
	//just to see some data
	SDL_Delay(1000);
	IMG_Quit();
	TTF_Quit();
	SDL_Quit();
}
void GameEngine::setCurrentScene(Scene* scene)
{
	delete GameEngine::currentScene;
	GameEngine::currentScene = scene;
	GameEngine::currentScene->init();
}

void GameEngine::start()
{
	this->running = true;
	this->gameloop();
}
void GameEngine::stop()
{
	this->running = false;
}
void GameEngine::gameloop()
{
	Uint32 nextGameTick = SDL_GetTicks();
	int loops = 0;
	int frames = 0;
	int ticks = 0;

	Uint32 lastTimer = SDL_GetTicks();

	Uint32 oldTime = SDL_GetTicks();

	Uint32 tickOld = SDL_GetTicks();

	SDL_Event e;

	SDL_StartTextInput();
	while(this->running)
	{
		//Delay loop
		SDL_Delay(1);

		//Handle all SDL Events
		GameEngine::eventMng->handleEvents(e);
		//this->handleEvents(&e);

		//Checks if it is time to tick
		loops = 0;
		while(SDL_GetTicks() > nextGameTick && loops < 5)
		{
			tick();
			nextGameTick += skipTicks;
			loops++;
			ticks++;
			GameEngine::tickDelta = SDL_GetTicks() - tickOld;
			tickOld = SDL_GetTicks();
		}
		//Calculates where in time we are in the tick
		double interpolation = (SDL_GetTicks() + skipTicks - nextGameTick) / (double) skipTicks;

		render(interpolation);
		frames++;

		delta = SDL_GetTicks() - oldTime;

		//Prints fps and ticks per seconds
		if(SDL_GetTicks() >= lastTimer)
		{
			//std::cout << "Fps: " << frames << " Ticks " << ticks << std::endl;
			std::string title = ("fps:" + std::to_string(frames) + " ticks:" + std::to_string(ticks));
			SDL_SetWindowTitle(this->window, title.c_str());
			frames = 0;
			ticks = 0;
			lastTimer += 1000;
		}
		oldTime = SDL_GetTicks();
	}
	SDL_StopTextInput();
	delete this;
}
void GameEngine::handleEvents(SDL_Event* e)
{
}
void GameEngine::tick()
{
	//Update current scene entity list
	if(GameEngine::currentScene != nullptr)
	{
		GameEngine::currentScene->tick();
	}
	GameEngine::eventMng->tick();

	//Update other things?
}
void GameEngine::render(double i)
{
	//Clear renderer
	SDL_SetRenderDrawColor(this->renderer, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderClear(this->renderer);

	//Fill "game area" with black
	SDL_Rect rect = { 0, 0, this->gameWidth, this->gameHeight };
	SDL_SetRenderDrawColor(this->renderer, 0x00, 0x00, 0x00, 0xFF);
	SDL_RenderFillRect(this->renderer, &rect);

	//Render current scene entity list
	if(GameEngine::currentScene != nullptr)
	{
		GameEngine::currentScene->render(this->renderer, i);
	}
	//Show renderer
	SDL_RenderPresent(this->renderer);
}
void GameEngine::renderText(int x, int y, const char* text, SDL_Color color, int size)
{
	TTF_Font* font = TTF_OpenFont("assets/ARIAL.TTF", size);

	SDL_Surface* tSurface = TTF_RenderText_Solid(font, text, color);
	SDL_Rect srect = { 0, 0, tSurface->w, tSurface->h };
	SDL_Texture* tTexture = SDL_CreateTextureFromSurface(this->renderer, tSurface);
	SDL_FreeSurface(tSurface);
	TTF_CloseFont(font);
	SDL_Rect drect = { x, y, srect.w, srect.h };
	SDL_RenderCopy(this->renderer, tTexture, &srect, &drect);

	SDL_DestroyTexture(tTexture);
}
